<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Trabalho C�lculo Num�rico - Parte 2</title>
<head>
</head>
<body>

<strong>Sistema de c�lculo num�rico</strong><br />
M�todo utilizado: Gauss-Seidel<br /><br />
Disciplina: C�lculo num�rico.<br />
Professor: Daniel Pezzi.<br />
Acad�mico: F�bio Duarte de Souza.<br /><br />



<?php	
	
/*

	Trabalho destinado a disciplina de C�lculo Num�rico.
	Professor: Daniel Pezzi
	Acad�mico: F�bio Duarte de Souza.
	Resolu��o de sistemas de equa��es - Gauss_Seidel
	
	5 1 1 | 5
	3 4 1 | 6
	3 3 6 | 0

*/

$linha_1 = array(5,1,1,5);
$linha_2 = array(3,4,1,6);
$linha_3 = array(3,3,6,0);

$aux = array();
$erro = 0.1;

//Valores iniciais
$x0 = 0;
$y0 = 0;
$z0 = 0;

// Fun��es
//Mostra valores na tabela
function mostra_valores($linha_1, $linha_2, $linha_3, $msg){
	echo $msg."<br /><table border='1'>";	
	echo "<tr align= 'center'>
		<td>Descri��o</td>
		<td>a11</td>
		<td>a12</td>
		<td>a13</td>
		<td>b1</td>
		</tr>
		<tr>
			<td>Valores da linha 1 
			</td>
			";
	$li = count($linha_1);	
	for($i=0; $i < $li; $i++){			
	echo "<td><strong>".$linha_1[$i]. "</strong></td>";	
	}
	echo "
		<tr>
			<td>Valores da linha 2 
			</td>
			";
	$li = count($linha_2);	
	for($i=0; $i < $li; $i++){			
	echo "<td><strong>".$linha_2[$i]. "</strong></td>";	
	}
	
	echo "
		<tr>
			<td>Valores da linha 3 
			</td>
			";
	$li = count($linha_3);	
	for($i=0; $i < $li; $i++){			
	echo "<td><strong>".$linha_3[$i]. "</strong></td>";	
	}
	echo " </tr>
		</table><br />";	
}
//Realiza o calculo da linha 1
function calculo_linha_1($l){
	$res = (abs($l[1]) + abs($l[2]))/ abs($l[0]);
	return $res;
}
//Realiza o calculo da linha 2
function calculo_linha_2($l){	
	$res = (abs($l[0]) + abs($l[2]))/ abs($l[1]);
	return $res;
}
//Realiza o calculo da linha 3
function calculo_linha_3($l){
	$res = (abs($l[0]) + abs($l[1]))/ abs($l[2]);
	return $res;
}
//Realiza o calculo a12
function calculo_a12($linha_1){
	$res = - ($linha_1[1] / $linha_1[0]);
	return $res;
}
//Realiza o calculo a13
function calculo_a13($linha_1){
	$res = - ($linha_1[2] / $linha_1[0]);
	return $res;
}
//Realiza o calculo b1
function calculo_b1($linha_1){
	$res = ($linha_1[3] / $linha_1[0]);
	return $res;
}
//Realiza o calculo a21
function calculo_a21($linha_2){
	$res = - ($linha_2[0] / $linha_2[1]);
	return $res;
}
//Realiza o calculo a23
function calculo_a23($linha_2){
	$res = - ($linha_2[2] / $linha_2[1]);
	return $res;
}
//Realiza o calculo b2
function calculo_b2($linha_2){
	$res = ($linha_2[3] / $linha_2[1]);
	return $res;
}
//Realiza o calculo a31
function calculo_a31($linha_3){
	$res = - ($linha_3[0] / $linha_3[2]);
	return $res;
}
//Realiza o calculo a32
function calculo_a32($linha_3){
	$res = - ($linha_3[1] / $linha_3[2]);
	return $res;
}
//Realiza o calculo b3
function calculo_b3($linha_3){
	$res = ($linha_3[3] / $linha_3[2]);
	return $res;
}
//Verifica qual o maior erro relativo encontrado
function maior_erro($ERx, $ERy, $ERz){
	if(($ERx > $ERy) || ($ERx > $ERz)){
		$maior = $ERx;
	}else if(($ERy > $ERx) || ($ERy > $ERz)){
		$maior = $ERy;
	}else if(($ERz > $ERx) || ($ERz > $ERy)){
		$maior = $ERz;
	}else{
		$maior = $ERx;
	}	
	return $maior;
}
//Verifica qual o maior valor da incognita
function maior_inco($x, $y, $z){
	if(($x > $y) || ($x > $z)){
		$maior = $x;
	}else if(($y > $x) || ($y > $z)){
		$maior = $y;
	}else if(($z > $x) || ($z > $y)){
		$maior = $z;
	}else{
		$maior = $x;
	}
	return $maior;
}

mostra_valores($linha_1, $linha_2, $linha_3, $msg = "Valores Iniciais");

//Calcula primeira linha
$teste_linha_1 = calculo_linha_1($linha_1);

echo "Resultado converg�ncia da linha 1 = <strong>". $teste_linha_1. "</strong><br />";

//calcula segunda linha
$teste_linha_2 = calculo_linha_2($linha_2);

echo "Resultado converg�ncia da linha 2 = <strong>". $teste_linha_2. "</strong><br />";

//calcula terceira linha
$teste_linha_3 = calculo_linha_3($linha_3);

echo "Resultado converg�ncia da linha 3 = <strong>". $teste_linha_3. "</strong><br />";

while((($teste_linha_1 > 1) && ($teste_linha_2 > 1)) || (($teste_linha_1 > 1) && ($teste_linha_3 > 1)) || (($teste_linha_2 > 1) && ($teste_linha_3 > 1))){
if(($teste_linha_1 > 1) && ($teste_linha_2 > 1)){
	echo "<br />Feito a troca das linhas 1 e 2.<br /><br />";
	$aux = $linha_1;
	$linha_1 = $linha_2;
	$linha_2 = $aux;	
}else if(($teste_linha_1 > 1) && ($teste_linha_3 > 1)){
	echo "<br />Feito a troca das linhas 1 e 3.<br /><br />";
	$aux = $linha_1;
	$linha_1 = $linha_3;
	$linha_3 = $aux;	
}else if(($teste_linha_2 > 1) && ($teste_linha_3 > 1)){
	echo "<br />Feito a troca das linhas 2 e 3.<br /><br />";
	$aux = $linha_2;
	$linha_2 = $linha_3;
	$linha_3 = $aux;	
}else{
	echo "N�o � preciso fazer permuta��o entre linhas.";
}
//Calcula primeira linha
$teste_linha_1 = calculo_linha_1($linha_1);

echo "Resultado converg�ncia da linha 1 = ". $teste_linha_1. "<br />";

//calcula segunda linha
$teste_linha_2 = calculo_linha_2($linha_2);

echo "Resultado converg�ncia da linha 2 = ". $teste_linha_2. "<br />";

//calcula terceira linha
$teste_linha_3 = calculo_linha_3($linha_3);

echo "Resultado converg�ncia da linha 3 = ". $teste_linha_3. "<br /><br />";

mostra_valores($linha_1, $linha_2, $linha_3, $msg = "Valores depois da permuta��o entre linhas.");

}

$linha_1_atual = array(0,calculo_a12($linha_1),calculo_a13($linha_1),calculo_b1($linha_1));
$linha_2_atual = array(calculo_a21($linha_2),0,calculo_a23($linha_2),calculo_b2($linha_2));
$linha_3_atual = array(calculo_a31($linha_3),calculo_a32($linha_3),0,calculo_b3($linha_3));
echo "<br />";	
mostra_valores($linha_1_atual, $linha_2_atual, $linha_3_atual, $msg = "Valores depois do c�lculo.");

$ER = 1; //inicia erro em 1
$i = 1;

while($ER >= $erro){
//calculando os valores de x, y,z
$x = ($linha_1_atual[0]) * ($x0) + ($linha_1_atual[1]) * ($y0) + ($linha_1_atual[2]) * ($z0) + ($linha_1_atual[3]);
$y = ($linha_2_atual[0]) * ($x) + ($linha_2_atual[1]) * ($y0) + ($linha_2_atual[2]) * ($z0) + ($linha_2_atual[3]);
$z = ($linha_3_atual[0]) * ($x) + ($linha_3_atual[1]) * ($y) + ($linha_3_atual[2]) * ($z0) + ($linha_3_atual[3]);

//Mostra a formula utilizada com os respectivos valores
echo "x$i = ($linha_1_atual[0]) * ($x0) + ($linha_1_atual[1]) * ($y0) + ($linha_1_atual[2]) * ($z0) + ($linha_1_atual[3]) = <strong>$x</strong><br />";
echo "y$i = ($linha_2_atual[0]) * ($x) + ($linha_2_atual[1]) * ($y0) + ($linha_2_atual[2]) * ($z0) + ($linha_2_atual[3]) = <strong>$y</strong><br />";
echo "z$i = ($linha_3_atual[0]) * ($x) + ($linha_3_atual[1]) * ($y) + ($linha_3_atual[2]) * ($z0) + ($linha_3_atual[3]) = <strong>$z</strong><br /><br />";

//Guarda o maior valor da incognita
$m_i = maior_inco($x, $y, $z);

//Calcula o erro relativo de cada variavel (x, y, z);
$ERx = abs(($x0 - $x) / $x);
$ERy = abs(($y0 - $y) / $y);
$ERz = abs(($z0 - $z) / $z);

//Mostra a formula utilizada com os respectivos valores
echo "ERx$i = abs(($x0 - $x) / $x) = <strong>$ERx</strong><br />";
echo "ERy$i = abs(($y0 - $y) / $y) = <strong>$ERy</strong><br />";
echo "ERz$i = abs(($z0 - $z) / $z) = <strong>$ERz</strong><br /><br />";

//Calcula o maior erro encontrado
$m_e = maior_erro($ERx, $ERy, $ERz);

//Verifica o erro para a condi��o de repeti��o
$ER = abs($m_e / $m_i);

//Guarda os valores encontrados para utilizar novamente na opera��o de repeti��o
$x0 = $x;
$y0 = $y;
$z0 = $z;
$i++;
echo "-------------------------------------------------------------------------------------------------------------------<br />";
};
$i = $i-1;
echo "Solu��o: ".$i . " Itera��es.";
?>
<table border="1">
<tr>
	<td>x<?php echo $i; ?></td>
	<td><strong><?php echo $ERx; ?></strong></td>
</tr>
<tr>
	<td>y<?php echo $i; ?></td>
	<td><strong><?php echo $ERy; ?></strong></td>
</tr>
<tr>
	<td>z<?php echo $i; ?></td>
	<td><strong><?php echo $ERz; ?></strong></td>
</tr>
</table>
</body>
</html>