<?php

/*
	Trabalho destinado a disciplina de C�lculo Num�rico.
	Professor: Daniel Pezzi
	Acad�mico: F�bio Duarte de Souza.
	Integra��o Num�rica - Regra de Simpson

	Intervalo incial : 1
	Intervalo final : 2,8
	N�mero de intervalos: 6
	Fun��o : exp(-x)

*/

$a = 1; //intervalo inicial
$b = 2.8;//intervalo final
$n = 6;//numero de intervalos
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Trabalho C�lculo Num�rico - Parte 2</title>
<head>
</head>
<body>

<strong>Sistema de c�lculo num�rico</strong><br />
M�todo utilizado: Simpson<br /><br />
Disciplina: C�lculo num�rico.<br />
Professor: Daniel Pezzi.<br />
Acad�mico: F�bio Duarte de Souza.<br /><br />

Intervalo inicial: <strong><?php echo $a; ?></strong><br />
Intervalo final: <strong><?php echo $b; ?></strong><br />
Intervalos: <strong><?php echo $n; ?></strong><br />
Fun��o: <strong>exp(x)</strong><br /><br />

<?php 


//Fun��o para achar a altura
function h($a, $b, $n){
	//Verifica se o valor do intervalo � par. Se n�o for um n�mero par, o c�lculo � encerrado.
	if(($n % 2) != 0){
		echo "Valor �mpar para n�mero de intervalos utilizando Simpson.";
		exit();
	}
	$t = ($b - $a) / $n;
	return $t;
}

//Fun��o que acha o valor de acordo com o valor do x
function funcao($x, $n){
	for($i=0; $i<=$n; $i++){
		$f[$i] = exp(-$x[$i]);
	}
	return $f;	
}

//Inseri os valores calculados em um array 
function intervalos($a, $b, $n, $h){
	$x = array();
	echo "Valor do x a cada itera��o. <br />";
	for($i=0; $i <= $n; $i++){
		if($i == 0){
			$x[$i] = $a;
		}else if($i == $n){
			$x[$i] = $b;
		}else{
			$j = ($i - 1);
			$x[$i] = $x[$j] + $h;			
		}		
		echo "x[$i] = <strong>$x[$i]</strong> <br />";
	}
	return $x;
}

//Calcula a altura
$h = h($a, $b, $n);
echo "Valor da altura encontrado = <strong>".$h. "</strong><br /><br />";

//Calcula os valores dos intervalos
$x = intervalos($a, $b, $n, $h);

//Executa a fun��o de calculo com os respectivos valores
$y = funcao($x, $n);

//Executa a f�rmula de simpson
$funcao = ($h/3) * ($y[0] + (4*$y[1]) + (2*$y[2]) + (4*$y[3]) + (2*$y[4]) + (4*$y[5]) + $y[6]);
echo "<br/>Fun��o utilizada <br/>($h/3) * ($y[0] + (4*$y[1]) + (2*$y[2]) + (4*$y[3]) + (2*$y[4]) + (4*$y[5]) + $y[6])";
echo "<br /><br />Valor encontrado = <strong>" .$funcao. "</strong>";
?>

</body>
</html>